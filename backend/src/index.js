import 'dotenv/config';
import cors from 'cors';
import bodyParser from 'body-parser';
import express from 'express';
import models, { sequelize } from './models';
import routes from './routes';

const OktaJwtVerifier = require('@okta/jwt-verifier');
const oktaJwtVerifier = new OktaJwtVerifier({
  clientId: process.env.REACT_APP_OKTA_CLIENT_ID,
  issuer: `${process.env.REACT_APP_OKTA_ORG_URL}/oauth2/default`,
});

const app = express();

// Application-Level Middleware

app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(async (req, res, next) => {
  req.context = { 
    models,
  };
  try {
    if (!req.headers.authorization) throw new Error('Authorization header is required');

    const accessToken = req.headers.authorization.trim().split(' ')[1];
    await oktaJwtVerifier.verifyAccessToken(accessToken);
    next();
  } catch (error) {
    next(error.message);
  }
});

// Routes

app.use('/contacts', routes.contact);
app.use('/numbers', routes.number);

// Start

const eraseDatabaseOnSync = false;

sequelize.sync({ force: eraseDatabaseOnSync }).then(async () => {
  if (eraseDatabaseOnSync) {
    createContactsWithNumbers();
  }
  const port = process.env.SERVER_PORT || 3001;
  app.listen(port || 3001, () =>
    console.log(`listening on port ${port}!`),
  );
});

const createContactsWithNumbers = async () => {
  await models.Contact.create(
    {
      contact_name: 'john cox',
      numbers: [
        {
          contact_number: '2505803048',
          contact_number_type: 'cell',
        },
        {
          contact_number: '5555555555',
          contact_number_type: 'home',
        },
      ],
    },
    {
      include: [models.Number],
    },
  );

  await models.Contact.create(
    {
      contact_name: 'naomi hodge',
      numbers: [
        {
          contact_number: '7788705085',
          contact_number_type: 'cell',
        },
      ],
    },
    {
      include: [models.Number],
    },
  );
};