import { Router } from 'express';

const router = Router();

router.get('/', async (req, res) => {
  const numbers = await req.context.models.Number.findAll();
  return res.send(numbers);
});

router.get('/:numberId', async (req, res) => {
  const number = await req.context.models.Number.findByPk(
    req.params.numberId,
  );
  return res.send(number);
});

router.put('/:numberId', async (req, res) => {
  const number = await req.context.models.Number.update(
    { 
      contact_number: req.body.number,
      contact_type: req.body.type,
    },
    { where: {id: req.params.numberId} }
  )

  return res.send(number);
});


router.delete('/:numberId', async (req, res) => {
  const result = await req.context.models.Number.destroy({
    where: { id: req.params.numberId },
  });

  return res.send(true);
});

export default router;