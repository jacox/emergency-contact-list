import { Router } from 'express';

const router = Router();

router.get('/', async (req, res) => {
  const contacts = await req.context.models.Contact.findAll({
    include: [
      {
        model: req.context.models.Number,
      }
    ]
  });
  return res.send(contacts);
});

router.get('/:contactId', async (req, res) => {
  const contact = await req.context.models.Contact.findByPk(
    req.params.contactId,
  );
  return res.send(contact);
});

router.post('/', async (req, res) => {
  const number = await req.context.models.Contact.create(
    { 
      contact_name: req.body.name,
      contact_role: req.body.role,
      numbers: req.body.numbers,
    },
    {
      include: [req.context.models.Number],
    },
  )

  return res.send(number);
});

router.put('/:contactId', async (req, res) => {
  const number = await req.context.models.Contact.update(
    { 
      contact_name: req.body.name,
      contact_role: req.body.role,
      numbers: req.body.numbers,
    },
    {
      where: { id: req.params.contactId }
    },
  )

  return res.send(number);
});

router.delete('/:contactId', async (req, res) => {
  const result = await req.context.models.Contact.destroy({
    where: { id: req.params.contactId },
  });

  return res.send(true);
});

export default router;