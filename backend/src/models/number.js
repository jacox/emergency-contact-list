const number = (sequelize, DataTypes) => {
    const Number = sequelize.define('number', {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        allowNull: false,
        primaryKey: true
      },
      contact_number: {
        type: DataTypes.STRING(30),
        allowNull: false,
      },
      contact_number_type: {
        type: DataTypes.STRING(50),
      },
    });
  
    Number.associate = models => {
      Number.belongsTo(models.Contact);
    };
  
    return Number;
  };
  
  export default number;