const contact = (sequelize, DataTypes) => {
  const Contact = sequelize.define('contact', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV1,
      allowNull: false,
      primaryKey: true
    },
    contact_name: {
      type: DataTypes.STRING(200),
      allowNull: false,
    },
    contact_role: {
      type: DataTypes.STRING(50),
    },
  });

  Contact.associate = models => {
    Contact.hasMany(models.Number, { onDelete: 'CASCADE' });
  };

  Contact.findNumber = async number => {
    let contact = await Contact.findOne({
      where: { id: login },
    });

    if (!contact) {
      contact = await Contact.findOne({
        where: { email: login },
      });
    }

    return contact;
  };

  return Contact;
};

export default contact;