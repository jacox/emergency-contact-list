import React from 'react';
import { Link } from 'react-router-dom';
import {
  AppBar,
  Button,
  Toolbar,
  Typography,
  withStyles,
} from '@material-ui/core';
import LoginButton from './LoginButton';
const styles = {
  flex: {
    flex: 1,
  },
};

const Header = ({ classes }) => (
  <AppBar position="static">
    <Toolbar>
      <Typography variant="title" color="inherit">
        Help Desk Emergency Contact List
      </Typography>
      <Button color="inherit" component={Link} to="/">Home</Button>
      <div className={classes.flex}/>
      <LoginButton />
    </Toolbar>
  </AppBar>
);

export default withStyles(styles)(Header);