import React, { Component, Fragment } from 'react';
import { withAuth } from '@okta/okta-react';
import { withRouter, Route, Link } from 'react-router-dom';
import {
  withStyles,
  Typography,
  Button,
  IconButton,
  Paper,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
} from '@material-ui/core';
import { Delete as DeleteIcon, Add as AddIcon } from '@material-ui/icons';
import { find, orderBy } from 'lodash';
import { compose } from 'recompose';

import ContactEditor from '../components/ContactEditor';

const styles = theme => ({
  contacts: {
    marginTop: 2 * theme.spacing.unit,
  },
  fab: {
    position: 'absolute',
    bottom: 3 * theme.spacing.unit,
    right: 3 * theme.spacing.unit,
    [theme.breakpoints.down('xs')]: {
      bottom: 2 * theme.spacing.unit,
      right: 2 * theme.spacing.unit,
    },
  },
});

const API = process.env.REACT_APP_API || 'http://localhost:3000';

class ContactsManager extends Component {
  state = {
    loading: true,
    contacts: [],
  };

  componentDidMount() {
    this.getContacts();
  }

  async fetch(method, endpoint, body) {
    try {
      const response = await fetch(`${API}${endpoint}`, {
        method,
        body: body && JSON.stringify(body),
        headers: {
          'content-type': 'application/json',
          accept: 'application/json',
          authorization: `Bearer ${await this.props.auth.getAccessToken()}`,
        },
      });
      return await response.json();
    } catch (error) {
      console.error(error);
    }
  }

  async getContacts() {
    this.setState({ loading: false, contacts: await this.fetch('get', '/contacts') });
  }

  saveContact = async (contact) => {
    if (contact.id) {
      await this.fetch('put', `/contacts/${contact.id}`, contact);
    } else {
      await this.fetch('post', '/contacts', contact);
    }

    this.props.history.goBack();
    this.getContacts();
  }

  async deleteContact(contact) {
    if (window.confirm(`Are you sure you want to delete "${contact.title}"`)) {
      await this.fetch('delete', `/contacts/${contact.id}`);
      this.getContacts();
    }
  }

  renderContactEditor = ({ match: { params: { id } } }) => {
    if (this.state.loading) return null;
    const contact = find(this.state.contacts, { id: Number(id) });

    //if (!contact && id !== 'new') return <Redirect to="/contacts" />;

    return <ContactEditor contact={contact} onSave={this.saveContact} />;
  };

  render() {
    const { classes } = this.props;

    return (
      <Fragment>
        <Typography variant="display1">Emergency Contacts</Typography>
        {this.state.contacts.length > 0 ? (
          <Paper elevation={1} className={classes.contacts}>
            <List>
              {orderBy(this.state.contacts, ['updatedAt', 'contact_name'], ['desc', 'asc']).map(contact => (

                <ListItem key={contact.id} button component={Link} to={`/contacts/${contact.id}`}>
                  
                  <ListItemText
                    primary={contact.numbers.length > 0? (`${contact.contact_name} ${contact.numbers[0].contact_number}`): (contact.contact_name)}
                    secondary={contact.contact_role}
                  />
                  <ListItemSecondaryAction>
                    <IconButton onClick={() => this.deleteContact(contact)} color="inherit">
                      <DeleteIcon />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              ))}
            </List>
          </Paper>
        ) : (
          !this.state.loading && <Typography variant="subheading">No contacts to display</Typography>
        )}
        <Button
          variant="fab"
          color="secondary"
          aria-label="add"
          className={classes.fab}
          component={Link}
          to="/contacts/new"
        >
          <AddIcon />
        </Button>
        <Route exact path="/contacts/:id" render={this.renderContactEditor} />
      </Fragment>
    );
  }
}

export default compose(
  withAuth,
  withRouter,
  withStyles(styles),
)(ContactsManager);